﻿using System;

namespace SOG.Backend.Database
{
    public struct QueryLogEntry
    {
        public int ThreadId;
        public int ExecutionTimeMs;
        public int ResultCount;
        public bool Success;
        public bool Update;
        public string CollectionName;
        public string Query;
        public string ExecutionDate;

        public static string FormattedDateNow
        {
            get { return DateTime.Now.ToString("yyyyMMddHHmmssFFF"); }
        }

        public override string ToString()
        {
            return $"[{ExecutionDate}][{CollectionName}][#{ThreadId}][{ExecutionTimeMs}ms]: {Query} numrows:{ResultCount} success:{Success}";
        }
    }
}
