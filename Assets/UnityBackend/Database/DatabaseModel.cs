﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Linq;

namespace SOG.Backend.Database.Models
{
    public static class AttributeExtensions
    {
        public static TValue GetAttributeValue<TAttribute, TValue>(
            this Type type,
            Func<TAttribute, TValue> valueSelector)
            where TAttribute : Attribute
        {
            var att = type.GetCustomAttributes(
                typeof(TAttribute), true
            ).FirstOrDefault() as TAttribute;
            if (att != null)
            {
                return valueSelector(att);
            }
            return default(TValue);
        }
    }

    public class MongoModel : Attribute
    {
        public string CollectionName;
        public MongoModel(string collectionName)
        {
            CollectionName = collectionName;
        }
    }

    public class DatabaseModel
    {
        public ObjectId Id { get; set; }
    }
    
    public class DatabaseModelVersioned : DatabaseModel
    {
        public int __v { get; set; }
    }
    
    public class DatabaseModelLocking : DatabaseModel
    {
        public string LockKey { get; set; }
    }
}