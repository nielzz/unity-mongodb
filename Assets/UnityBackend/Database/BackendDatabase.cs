﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Threading.Tasks;
using System.Threading;
using System;
using SOG.Backend.Database.Models;

namespace SOG.Backend.Database
{
    public class BackendDatabase : Singleton<BackendDatabase>
    {

        public string MongoDBConnectionString = "mongodb://localhost";
        public string MongoDBDatabaseName;
        private MongoClient DatabaseClient;
        private MongoServer Server;
        public MongoDatabase Database;
        public int SimulateLatencyMilliseconds;

        public List<QueryLogEntry> QueryLog = new List<QueryLogEntry>();

        IEnumerator Start()
        {
            BsonDefaults.GuidRepresentation = GuidRepresentation.Standard;
            DatabaseClient = new MongoClient(new MongoUrl(MongoDBConnectionString));
            Server = DatabaseClient.GetServer();
            Server.Connect();
            while (Server.State != MongoServerState.Connected)
            {
                yield return null;
            }

            Debug.Log($"Connected to mongodb server running v.{Server.BuildInfo.VersionString} on {Server.BuildInfo.SysInfo} ");

            Database = Server.GetDatabase(MongoDBDatabaseName);
        }


        public void FindOne<T>(IMongoQuery query, Action<T> onFinished) where T : DatabaseModel
        {
            FindOne<T>(query).ContinueWith((Task<T> res) =>
            {
                onFinished(res.Result);
            });
        }

        public void Find<T>(IMongoQuery query, Action<List<T>> onFinished) where T : DatabaseModel
        {
            Find<T>(query).ContinueWith((Task<List<T>> res) =>
            {
                onFinished(res.Result);
            });
        }

        public void Save<T>(T updateDocument, UpdateFlags updateFlags = UpdateFlags.None, Action<T> onFinished) where T : DatabaseModel
        {
            Save<T>(updateDocument).ContinueWith((Task<T> res) =>
            {
                onFinished(updateDocument);
            });
        }

        public async Task<T> FindOne<T>(IMongoQuery query) where T : DatabaseModel
        {
            var result = await Find<T>(query);
            if (result.Count > 0)
                return result[0];
            return null;
        }

        public async Task<List<T>> Find<T>(IMongoQuery query) where T : DatabaseModel
        {
            return await Query<T>(query);
        }

        public async Task<T> Save<T>(T updateDocument, UpdateFlags updateFlags = UpdateFlags.None) where T : DatabaseModel
        {
            var result = await Query<T>(QueryShortcuts.GetById(updateDocument.Id.ToString()), updateDocument);
            // TODO: we can do better!
            return null;
        }

        private async Task<List<T>> Query<T>(IMongoQuery query, T updateData = null, UpdateFlags updateFlags = UpdateFlags.None) where T : DatabaseModel
        {
            var sw = System.Diagnostics.Stopwatch.StartNew();
            return await Task.Run<List<T>>(() =>
            {
                if (SimulateLatencyMilliseconds > 0)
                {
                    // simulate latency
                    Thread.Sleep(SimulateLatencyMilliseconds);
                }
                // TODO: cache attribute results
                var attrs = typeof(T).GetCustomAttributes(typeof(MongoModel), false);
                string foundCollectionName = string.Empty;
                foreach (System.Attribute attr in attrs)
                {
                    if (attr is MongoModel)
                    {
                        MongoModel a = (MongoModel)attr;
                        foundCollectionName = a.CollectionName;
                    }
                }
                if (!string.IsNullOrEmpty(foundCollectionName))
                {

                    if (updateData == null)
                    {
                        var result = Database.GetCollection<T>(foundCollectionName).Find(query);
                        List<T> results = new List<T>();
                        foreach (var item in result)
                        {
                            results.Add(item);
                        }
                        sw.Stop();
                        var logEntry = new QueryLogEntry()
                        {
                            ExecutionDate = QueryLogEntry.FormattedDateNow,
                            Success = true,
                            Query = query.ToString(),
                            CollectionName = foundCollectionName,
                            ExecutionTimeMs = (int)sw.ElapsedMilliseconds,
                            ThreadId = Thread.CurrentThread.ManagedThreadId,
                            ResultCount = results.Count,
                            Update = false
                        };
                        QueryLog.Add(logEntry);
                        Debug.Log(logEntry);
                        return results;
                    }
                    else
                    {
                        var result = Database.GetCollection<T>(foundCollectionName).Update(query, new UpdateDocument(updateData.ToBsonDocument()), updateFlags);
                        sw.Stop();
                        var logEntry = new QueryLogEntry()
                        {
                            ExecutionDate = QueryLogEntry.FormattedDateNow,
                            Success = result.Ok,
                            Query = query.ToString(),
                            CollectionName = foundCollectionName,
                            ExecutionTimeMs = (int)sw.ElapsedMilliseconds,
                            ThreadId = Thread.CurrentThread.ManagedThreadId,
                            Update = true
                        };
                        QueryLog.Add(logEntry);
                        Debug.Log(logEntry);
                        return null;
                    }

                }
                else
                {
                    Debug.LogError($"MongoModel attribute missing on {typeof(T).Name}, cannot determine collection name.");
                    return null;
                }
            });
        }
    }
}