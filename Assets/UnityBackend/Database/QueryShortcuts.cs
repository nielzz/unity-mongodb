﻿using MongoDB.Driver;
using MongoDB.Bson;

namespace SOG.Backend.Database
{
    public static class QueryShortcuts
    {
        public static QueryDocument GetById(string id, bool upsert = false)
        {
            var query = new QueryDocument("_id", new ObjectId(id));
            return query;
        }
    }
}