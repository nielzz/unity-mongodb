﻿using System.Collections;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using SOG.Backend.Database;
using SOG.Backend.Database.Models;
using UnityEngine;

public class PlayerSavegame : MonoBehaviour
{

	public string PlayerId;

	public PlayerProfileSavegame CurrentSavegame;
	
	void Start()
	{
		
	}

	public void LoadOrCreateSavegame(string id)
	{
		PlayerId = id;
		BackendDatabase.Instance.FindOne<PlayerProfileSavegame>(QueryShortcuts.GetById(id), savegame =>
			{
				if (savegame != null)
				{
					CurrentSavegame = savegame;
				}
				else
				{
					CurrentSavegame = GetNewSavegame();
				}
			});	
	}

	private PlayerProfileSavegame GetNewSavegame()
	{
		return new PlayerProfileSavegame();
	}

	public void SaveToDatabase()
	{
		BackendDatabase.Instance.Save(CurrentSavegame, UpdateFlags.Upsert, savegame =>
		{
			Debug.Log("Saved?");
		});
	}
}
