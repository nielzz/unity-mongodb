﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SOG.Backend.Database.Models
{
	public class InventoryEntry
	{
		public string Key;
		public int Count;
		public int Seed;
	}
	
	public class PlayerShipConfiguration
	{
		public string ShipModel;
		public string SailTexture;
		public ShipCanonConfiguration[] Canons;
	}
	
	public class ShipCanonConfiguration
	{
		public string Key;
	}

	[MongoModel("playerprofiles")]
	public class PlayerProfileSavegame : DatabaseModelVersioned
	{
		public string DisplayName;
		public string PlayerModel;
		public int Experience = 0;
		public bool Admin;
		public List<InventoryEntry> Inventory = new List<InventoryEntry>();
		public PlayerShipConfiguration PlayerShip;
	}
}
